package cz.pospech.arbes.converter

import cz.pospech.arbes.domain.PhoneLog
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

@Component
class PhoneLogConverter(): Converter<String,List<PhoneLog>> {

    companion object {
        private const val PATTERN = "dd-MM-yyyy HH:mm:ss"
        val FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern(PATTERN)
    }

    override fun convert(source: String): List<PhoneLog>? {
        return source.lines().map {
            val record = it.split(",")

            if(record.size != 3) {
                throw IllegalStateException("Invalid record size")
            }

            val numberString = record[0]
            val startTimeString = record[1]
            val endTimeString = record[2]

            val startTime = LocalDateTime.parse(startTimeString, FORMATTER)
            val endTime = LocalDateTime.parse(endTimeString, FORMATTER)

            PhoneLog(
                number = numberString,
                startTime = startTime,
                startTimeTruncated = startTime.truncatedTo(ChronoUnit.MINUTES),
                endTime = endTime,
                endTimeTruncated = endTime.truncatedTo(ChronoUnit.MINUTES).plusMinutes(1),
                duration = startTime.until(endTime, ChronoUnit.MINUTES) + 1
            )
        }
    }
}