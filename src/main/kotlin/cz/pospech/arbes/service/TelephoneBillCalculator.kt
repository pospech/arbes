package cz.pospech.arbes.service

import cz.pospech.arbes.converter.PhoneLogConverter
import cz.pospech.arbes.domain.PhoneLog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.LocalTime
import java.time.temporal.ChronoUnit

interface TelephoneBillCalculator {

    /**
     * Calculates price for phone.
     * Calculation rules:
     * - interval <8:00:00,16:00:00) -> billed 1CZK/min, outside -> 0.5 CZK/min (selected by starting minute)
     * - for longer than 5 minutes call -> 0.2 CZK/min after first 5 minutes (interval independent)
     * - promo action: mostly called number is free of charges (if multiple found, the one with longer call time is selected)
     *
     * @param phoneLog CSV structured phone log in format: NORMALIZED_PHONE_NUMBER;START_TIME;END_TIME
     *
     * @return Price to bay for the phone log
     */
    fun calculate(phoneLog: String): BigDecimal
}

@Service
class TelephoneBillCalculatorImpl(
    @Value("\${arbes.pricing.low.duration}")
    private val lowPricingDuration: Long,
    @Value("\${arbes.pricing.low.amount}")
    private val lowPricingAmount: BigDecimal,
    @Value("#{T(java.time.LocalTime).parse('\${arbes.interval.start}', T(java.time.format.DateTimeFormatter).ofPattern('HH:mm:ss'))}")
    private val intervalStart: LocalTime,
    @Value("#{T(java.time.LocalTime).parse('\${arbes.interval.end}', T(java.time.format.DateTimeFormatter).ofPattern('HH:mm:ss'))}")
    private val intervalEnd: LocalTime,
    @Value("\${arbes.pricing.interval.inside}")
    private val intervalPrice: BigDecimal,
    @Value("\${arbes.pricing.interval.outside}")
    private val nonIntervalPrice: BigDecimal,
    @Autowired
    private val converter: PhoneLogConverter
) : TelephoneBillCalculator {

    override fun calculate(phoneLog: String): BigDecimal {
        val logs =
            converter.convert(phoneLog) ?: throw IllegalStateException("Unexpected conversion of phone log to null")

        val promotedFiltered = filterPromotedNumber(logs)

        return promotedFiltered
            .sumOf { getLogPrice(it) }
    }

    /**
     * Calculates price for this particular log
     *
     * @param log Call to be calculated
     *
     * @return Price for the call
     */
    private fun getLogPrice(log: PhoneLog): BigDecimal {
        val minutesPrice = getMinutesPrice(log)

        return minutesPrice.sumOf { it }
    }

    /**
     * Gets list with prices for each started minute in the call
     *
     * @param log Call to be create minutes price list for
     *
     * @return List of proces for each started minute of call
     */
    private fun getMinutesPrice(log: PhoneLog): List<BigDecimal> {
        val result = mutableListOf<BigDecimal>()

        var timePointer = log.startTime

        for (i in 1..log.duration) {
            if (i > lowPricingDuration) {
                result.add(lowPricingAmount)
            } else {
                result.add(getPointerPrice(timePointer.toLocalTime()))
                timePointer = timePointer.plusMinutes(1)
            }
        }

        return result
    }


    /**
     * Gets price of particular minute in configured time interval.
     * Minute must be before end of interval and equal or after start of interval to charge full price,
     * non interval price is charged otherwise.
     *
     * @param pointer Pointer to one specific place in time (start of the minute)
     *
     * @return Price of particular minute of the call
     */
    private fun getPointerPrice(pointer: LocalTime): BigDecimal {
        return if (!pointer.isBefore(intervalStart) && pointer.isBefore(intervalEnd)) {
            intervalPrice
        } else {
            nonIntervalPrice
        }

    }

    /**
     * Filters out promoted number in [original] list of phone calls.
     * Number with the most calls (first with highest call duration) is filtered out
     *
     * @param original Original list of phone calls
     *
     * @return Filtered list of phone calls.
     */
    private fun filterPromotedNumber(original: List<PhoneLog>): List<PhoneLog> {
        val numbersCounted = original
            .groupingBy { it.number }
            .eachCount()

        val promotedNumbers = numbersCounted
            .filter { it.value == numbersCounted.values.maxOrNull() }
            .keys

        if (promotedNumbers.size == 1) {
            return original.filter { it.number != promotedNumbers.first() }
        }

        val promotedNumbersGrouped = original
            .filter { promotedNumbers.contains(it.number) }
            .groupBy { it.number }

        val promotedNumbersDuration = promotedNumbersGrouped.map { promotedNumber ->
            promotedNumber.key to promotedNumber
                .value
                .sumBy { it.startTime.until(it.endTime, ChronoUnit.SECONDS).toInt() }
        }.toMap()

        val maxPromoted = promotedNumbersDuration.values.maxOrNull()
        val promotedNumber = promotedNumbersDuration.filter { it.value == maxPromoted }.keys.first()

        return original.filter { it.number != promotedNumber }
    }
}