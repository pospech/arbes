package cz.pospech.arbes

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ArbesApplication

fun main(args: Array<String>) {
	runApplication<ArbesApplication>(*args)
}
