package cz.pospech.arbes.domain

import java.time.LocalDateTime

/**
 * Representation of Phone Call
 *
 * @param number Called number
 * @param startTime Start time of the call
 * @param endTime End time of the call
 */
data class PhoneLog(
    val number: String,
    val startTime: LocalDateTime,
    val endTime: LocalDateTime,
    val duration: Long,
    val startTimeTruncated: LocalDateTime,
    val endTimeTruncated: LocalDateTime
)
