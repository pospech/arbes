package cz.pospech.arbes.controller

import cz.pospech.arbes.service.TelephoneBillCalculator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@RestController
class RestController {

    @Autowired
    private lateinit var calculator: TelephoneBillCalculator

    @PostMapping("")
    fun calculate(@RequestBody phoneLog: String):BigDecimal {
        return calculator.calculate(phoneLog)
    }
}