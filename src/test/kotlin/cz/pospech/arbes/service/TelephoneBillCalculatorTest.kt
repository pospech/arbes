package cz.pospech.arbes.service

import cz.pospech.arbes.converter.PhoneLogConverter
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.time.LocalTime

class TelephoneBillCalculatorTest {

    private val converter = PhoneLogConverter()

    private val underTest = TelephoneBillCalculatorImpl(
        lowPricingDuration = 5,
        lowPricingAmount = BigDecimal.valueOf(0.2),
        intervalStart = LocalTime.of(8, 0),
        intervalEnd = LocalTime.of(16, 0),
        intervalPrice = BigDecimal.ONE,
        nonIntervalPrice = BigDecimal.valueOf(0.5),
        converter = converter
    )

    @Test
    fun calculate_oneSingleCall_free() {
        val phoneLog = """
            420774577453,13-01-2020 14:10:15,13-01-2020 14:12:57
        """.trimIndent()

        val result = underTest.calculate(phoneLog)

        Assertions.assertEquals(BigDecimal.valueOf(0), result)
    }

    @Test
    fun calculate_multipleCallsInsideInterval_fullPrice() {
        val phoneLog = """
            420774577453,13-01-2020 14:10:15,13-01-2020 14:12:57
            420774577452,14-01-2020 14:10:15,14-01-2020 14:12:57
        """.trimIndent()

        val result = underTest.calculate(phoneLog)

        Assertions.assertEquals(BigDecimal.valueOf(3), result)
    }

    @Test
    fun calculate_multipleCallsStartsOutsideInterval_minuteOutside() {
        val phoneLog = """
            420774577453,13-01-2020 07:59:59,13-01-2020 08:02:00
            420774577452,14-01-2020 14:10:15,14-01-2020 14:12:57
        """.trimIndent()

        val result = underTest.calculate(phoneLog)

        Assertions.assertEquals(BigDecimal.valueOf(2.5), result)
    }

    @Test
    fun calculate_multipleCallsStartsOutsideIntervalLong_minuteOutside() {
        val phoneLog = """
            420774577453,13-01-2020 07:59:59,13-01-2020 08:06:00
            420774577452,14-01-2020 14:10:15,14-01-2020 14:20:57
        """.trimIndent()

        val result = underTest.calculate(phoneLog)

        Assertions.assertEquals(BigDecimal.valueOf(4.9), result)
    }

    @Test
    fun calculate_multipleCallsStartsInsideInterval_minuteInside() {
        val phoneLog = """
            420774577453,13-01-2020 15:59:59,13-01-2020 16:02:00
            420774577452,14-01-2020 14:10:15,14-01-2020 14:12:57
        """.trimIndent()

        val result = underTest.calculate(phoneLog)

        Assertions.assertEquals(BigDecimal.valueOf(2.0), result)
    }

    @Test
    fun calculate_multipleCallsStartsInsideIntervalLong_minuteInside() {
        val phoneLog = """
            420774577453,13-01-2020 15:59:59,13-01-2020 16:09:00
            420774577452,14-01-2020 14:10:15,14-01-2020 14:20:57
        """.trimIndent()

        val result = underTest.calculate(phoneLog)

        Assertions.assertEquals(BigDecimal.valueOf(4.0), result)
    }

    @Test
    fun calculate_multipleCallsInsideIntervalLonger_fullPrice() {
        val phoneLog = """
            420774577453,13-01-2020 14:10:15,13-01-2020 14:16:57
            420774577452,14-01-2020 14:10:15,14-01-2020 14:12:57
            420774577452,15-01-2020 14:10:15,15-01-2020 14:12:57
        """.trimIndent()

        val result = underTest.calculate(phoneLog)

        Assertions.assertEquals(BigDecimal.valueOf(5.4), result)
    }

    @Test
    fun calculate_multipleCallsOutsideIntervalLonger_lowerPrice() {
        val phoneLog = """
            420774577453,13-01-2020 16:10:15,13-01-2020 16:16:57
            420774577452,14-01-2020 14:10:15,14-01-2020 14:12:57
            420774577452,15-01-2020 14:10:15,15-01-2020 14:12:57
        """.trimIndent()

        val result = underTest.calculate(phoneLog)

        Assertions.assertEquals(BigDecimal.valueOf(2.9), result)
    }

    @Test
    fun calculate_multipleCalls_success() {
        val phoneLog = """
            420774577453,14-01-2020 10:10:15,14-01-2020 15:24:57
            420774577453,14-01-2020 10:10:15,14-01-2020 15:24:57
            420774577453,14-01-2020 10:10:15,14-01-2020 15:24:57
            420774577453,14-01-2020 10:10:15,14-01-2020 15:24:57
            420774577453,13-01-2020 16:10:15,13-01-2020 16:16:57
            420774577452,14-01-2020 14:10:15,14-01-2020 14:12:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577451,15-01-2020 14:10:15,15-01-2020 15:26:57
            420774577450,15-01-2020 14:10:15,15-01-2020 14:26:57
        """.trimIndent()

        val result = underTest.calculate(phoneLog)

        Assertions.assertEquals(BigDecimal.valueOf(281.3), result)
    }
}